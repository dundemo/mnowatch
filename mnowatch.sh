#!/bin/bash
#set -x
trap "exit 1" TERM
export TOP_PID=$$

# Licence: GPLv2
# The author of the software is the owner of the Dash Address: XnpT2YQaYpyh7F9twM6EtDMn1TCDCEEgNX
# Tweaking / Debugging by xkcd@dashtalk 
#
# MNOWATCH VERSION: 0.14
# COMPATIBLE TO DASHD VERSION 16 (works also for DASHD VERSION 12 and above)

#==========================INSTRUCTIONS ======================

# 1) Set MYHOME_DIR to whatever DIR you want, provided that the script has read/write priviledges there. 
#    example: MYHOME_DIR="/home/demo"
#    You may also leave the below as it is, in case you want everything to reside in $HOME.
MYHOME_DIR=$HOME
# 2) IMPORTANT: All MNOWATCH files must reside into $MYHOME_DIR/bin, so create that dir and put them inside it.
# 3) Run the script. It runs silently and lasts about 2 minutes (in an Intel Xeon 2.7 Ghz)
#    If the script ends withour errors, everything is fine. Check $MYHOME_DIR/httpd for new reports.
#    No new report will appear in case the previous report is identical.
# 4) Set SIMILARNUM less than 99 and greater than 0 in case you want to spot similarities.
#    WARNING: Setting $SIMILARNUM greater than 0 may cause HUGE delays in script's execution!
#    If you want to overwrite the default SIMILARNUM you can run: mnowatch.sh <number>
SIMILARNUM=0
# 5) If you want to connect to a remote dash-cli, set LOCAL_DASHCLI to 0 and edit the config file.
LOCAL_DASHCLI=0
#==========================END OF INSTRUCTIONS ==================

MYCONFIG_DIR=$MYHOME_DIR"/bin"
which dash-cli>/dev/null||{ echo "I dont know where the command dash-cli is. Please put dash-cli in your execution path.";exit;}
if [ $LOCAL_DASHCLI -eq 0 ]
then
 rpcuser=$(cut -f1 -d, $MYCONFIG_DIR/config.txt)
 rpcpassword=`cut -f2 -d, $MYCONFIG_DIR/config.txt`
 rpcconnect=`cut -f3 -d, $MYCONFIG_DIR/config.txt`
fi
dcli () {
 if [ $LOCAL_DASHCLI -eq 0 ]
 then
  dash-cli -datadir=/tmp -rpcuser=$rpcuser -rpcpassword=$rpcpassword -rpcconnect=$rpcconnect "$@" 2>&1||{ echo "The command dash-cli does not work remotely.";kill -s TERM $TOP_PID;}
 else
  dash-cli "$@" 2>&1||{ echo "I dont know where the command dcli is. Please put dcli in your execution path.";exit;}
 fi
}

which bc>/dev/null||{ echo "I dont know where the command bc is. Please put bc in your execution path.";exit;}

checkbin=$(cd $(dirname $0) &&pwd|grep /bin$|wc -l)

if [ $checkbin -eq 0 ]; then
test -n "$THIS_IS_CRON"||{ echo "INSTALLATION ERROR: Please put all the mnowatch gitlab files into the directory named "$MYHOME_DIR"/bin";}
exit 
fi 

BIN_DIR=$MYHOME_DIR"/bin"

TMP_DIR=$MYHOME_DIR"/tmp" 
if [ ! -d $TMP_DIR ] ; then 
 mkdir $TMP_DIR||{ echo "Unable to create directory $TMP_DIR. Please check your read-write priviledges.";exit 1;}
fi
#NOTE: EACHTIME A REPORT IS CREATED, WE INsERT IT IN THE 3rd row of index.html. The First 2 rows of index.html are reserved as shown below. You may change the 1st and second row, but keep space for the 3rd row to be inserted smoothly.
HTTPD_DIR=$MYHOME_DIR"/httpd" ; if [ ! -d $HTTPD_DIR ] ; then mkdir $HTTPD_DIR ; echo "<!DOCTYPE html><html lang=\"en\"><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><title>MNOwatch</title></head>" > $HTTPD_DIR/index.html ; echo "<body> Hello world. The time of the reports is UTC. <br>" >> $HTTPD_DIR/index.html ; echo '<!--  Everything below this line is updated by MNOWatch.sh, do not modify! -->' >> $HTTPD_DIR/index.html  ; echo "</body></html>" >> $HTTPD_DIR/index.html ; fi;

TYPES_DIR=$MYHOME_DIR"/httpd/Types" ; if [ ! -d $TYPES_DIR ] ; then mkdir $TYPES_DIR ; echo "<html><body>" > $TYPES_DIR/index.html ; echo "Here we explain the reason why the admins classified some masternodes into a specific type.<p> " >> $TYPES_DIR/index.html ; echo "</body></html>" >> $TYPES_DIR/index.html ; fi;

PREVIUSREPORT=`cd $HTTPD_DIR;ls -tra the_results_dashd_*.html.csv 2>/dev/null|tail -1`
PREVIUSREPORTCOUNT=`echo $PREVIUSREPORT|wc -c`
if [ $PREVIUSREPORTCOUNT -gt 1 ]
then
PREVIUSREPORTFULL=$HTTPD_DIR"/"$PREVIUSREPORT
else
PREVIUSREPORTFULL=""
fi

MYCOMMENT="" 
superblock=0
if [ $# -gt 0 ] ; then
  MYCOMMENT=$@ #comment is initialy all arguments. But then.....
  re='^[0-9]+$'
  if [[ $1 =~ $re ]] ; then
    if [[ $1 -ge 0 && $1 -lt 100 ]] ; then
      SIMILARNUM=$1
    fi
	MYCOMMENT=`echo $@|cut -f2- -d" "` #comment starts after the similarity number
  elif [ $1 == '-super' ] ; then
#BUG
#BUG in case the super report is identical to the previous report, not "--end of vote" tag appears in index.html
#BUG
    superblock=1
	MYCOMMENT=`echo $@|cut -f2- -d" "` # comment starts after the -super flag
    if [ $# -gt 1 ] ; then
      if [[ $2 =~ $re ]] ; then
        if [[ $2 -ge 0 && $2 -lt 100 ]] ; then
          SIMILARNUM=$2
        fi
	    MYCOMMENT=`echo $@|cut -f3- -d" "` #comment starts after the -super flag and the similarity number
      fi
    fi
  fi
fi

if [ $SIMILARNUM -gt 0 ]; then
which ssdeep>/dev/null||{ echo "I dont know where ssdeep command is. Please put ssdeep in your execution path.";exit;}
fi

codeurl="https://gitlab.com/dundemo/mnowatch"
codeurl2="You may find the code used to produce this report <a href=\""$codeurl"\"> here </a>. The time of the report is UTC. <br>"

if [ $superblock -gt 0 ]
then
codeurl2=$codeurl2" <strong>This Report occured as close as possible to the Votes Deadline</strong><br>"
fi

#put the below at the end of a the start?
#if [ $superblock -gt 0 ]
# then
#$BIN_DIR/types.sh `ls -tra $HTTPD_DIR/*.uniqueHashVotes.*.csv|tail -1`
#fi

for pid in $(pidof -x mnowatch.sh); do
    if [ $pid != $$ ]; then
        echo "[$(date)] : mnowatch.sh : Process is already running with PID $pid"
		#In case $superblock -gt 0 then put endOFvote tag in the previus report.
        exit 1
    fi
done

cd $TMP_DIR
cd $TMP_DIR; rm -rf *_* upload proposals


#BUG: https://app.crowdnode.io/ is more accurate! Use it, instead of crowdnode.io. Unfortunately it provides info about IPs rather than addresses, but we could fix that somehow.
#wget -qO- crowdnode.io|grep ">MN"|awk -F"address.dws" '{for(i=2;i<=NF;i++){{print $i}}}'|sed 's/.\(.\{34\}\).*/\1/g'|uniq > $HTTPD_DIR/Types/CrowdNode.txt
#code commented. Xkcd's code is responsible of creating $HTTPD_DIR/Types/CrowdNode.txt

dcli masternodelist|jq -r '.[]| "\(.collateraladdress) \(.address)"'|cut -f1 -d: > collateraladdress_IP
#HCMN
dcli masternodelist|jq -r '.[]| "\(.collateraladdress) \(.address) \(.type) \(.status)"' > collateraladdress_IPtype
#end HCMN

cat /dev/null > current_props
cat /dev/null > current_propshash
#xkcd version without dashcentral
nextsuperblockseconds=$(echo "($(dcli getgovernanceinfo|jq -r '.nextsuperblock') - $(dcli getblockcount))*2.625*60"|bc)
nextsuperblockseconds=$(printf "%.0f" $nextsuperblockseconds)
nextsuperblocktime=$((nextsuperblockseconds + EPOCHSECONDS))

#BUG due to the change of end_epoch into the Datastring
#dcli gobject list valid proposals|grep end_epoch|sort -nr -t: -k7|cut -f3-4 -d:|cut -f1-2 -d,|sed -e s/'\\"name\\":\\"'/''/g|sed -e s/'\\"'/''/g|while IFS=, read time name;do if ((time>nextsuperblocktime));then echo "$name">>current_props;fi;done
#Bug fix below
gobject=$(dcli gobject list valid proposals)
for hash in $(jq -r 'keys_unsorted|.[]' <<< "$gobject");do
    DataString=$(jq ".[] | select(.Hash == \"$hash\").DataHex"<<<"$gobject")
    DataString=$(eval dcli gobject deserialize $DataString)
    end_epoch=$(jq -r '.end_epoch'<<<"$DataString")
    name=$(jq -r '.name'<<<"$DataString")
    ((end_epoch>nextsuperblocktime))&& echo "$name"
done >current_props
#end bug fix

#end xkcd
#BUG due to the change of end_epoch into the Datastring
#dcli gobject list valid proposals|jq -r '.[]| "\(.DataString) \(.Hash)"'|cut -f1,2,7 -d,|sed -e s/'{"end_epoch":'/''/g|sed -e s/',"name":'/','/g|sed -e s/',"url":'/','/g|cut -f1,2,5 -d'"'|sed -e s/'"} '/','/g|sed -e s/'"'//g|while IFS=, read time name;do if ((time>nextsuperblocktime));then echo "$name">>current_propshash;fi;done
#Bug fix below
gobject=$(dcli gobject list valid proposals)
for hash in $(jq -r 'keys_unsorted|.[]' <<< "$gobject");do
    DataString=$(jq ".[] | select(.Hash == \"$hash\").DataHex"<<<"$gobject")
    DataString=$(eval dcli gobject deserialize $DataString)
    end_epoch=$(jq -r '.end_epoch'<<<"$DataString")
    name=$(jq -r '.name'<<<"$DataString")
    ((end_epoch>nextsuperblocktime))&& echo "${name},${hash}"
done >current_propshash
#end bug fix

echo  > expired_props

#dcli masternodelist addr|grep -v "[0:0:0:0:0:0:0:0]:0" > masternodelist_addr #https://github.com/dashpay/dash/issues/2942
dcli masternodelist addr|grep -v "[0:0:0:0:0:0:0:0]:0"|grep -v ': "\[::\]:0"' > masternodelist_addr #https://github.com/dashpay/dash/issues/2942
#BUG may be above in grep -v

dcli gobject list > gobject_list

#xkcd version
jq -r '.[].Hash' gobject_list > proposals

for fn in `cat proposals`; do
dcli gobject getcurrentvotes $fn > "gobject_getcurrentvotes_"$fn
#BUG due to the change of end_epoch into the Datastring
#prop=$(grep -B1 "\"Hash\": \""$fn gobject_list|head -1|cut -f4 -d":"|cut -f2 -d"\\"|sed 's/"//g')
prop=$(grep -B1 "\"Hash\": \""$fn gobject_list|head -1|sed 's/\\//g'|cut -f2- -d:|cut -f2- -d{|cut -f1 -d}|sed -e s/^/{/g|sed 's/.*/&}/'|jq -r|grep '"name":'|cut -f2- -d:|cut -f2 -d'"')
#end bug fix

greprop=`grep "^$prop$" current_props |wc -c`

if [ $greprop -gt 1 ]
then

propc=`echo $prop|wc -c`
if [ $propc -gt 1 -a $propc -lt 200 ]
then

#xkcd version
grep -i ABSTAIN:FUNDING "gobject_getcurrentvotes_"$fn|awk -F \: '{print $2}' > "ABSTAIN_"$prop

>"ABSTAIN_IP_"$prop
for gn in `cat "ABSTAIN_"$prop`; do
grep $gn\" masternodelist_addr|cut -f2 -d":"|cut -f2 -d"\"" >> "ABSTAIN_IP_"$prop
done
sort "ABSTAIN_IP_"$prop -o "ABSTAIN_IP_"$prop

#xkcd version
grep -i NO:FUNDING "gobject_getcurrentvotes_"$fn|awk -F \: '{print $2}' > "NO_"$prop

>"NO_IP_"$prop
for gn in `cat "NO_"$prop`; do
grep $gn\" masternodelist_addr|cut -f2 -d":"|cut -f2 -d"\"" >> "NO_IP_"$prop
done
sort "NO_IP_"$prop -o "NO_IP_"$prop

#xkcd version
grep -i YES:FUNDING "gobject_getcurrentvotes_"$fn|awk -F \: '{print $2}' > "YES_"$prop

>"YES_IP_"$prop
for gn in `cat "YES_"$prop`; do
grep $gn\" masternodelist_addr|cut -f2 -d":"|cut -f2 -d"\"" >> "YES_IP_"$prop
done
sort "YES_IP_"$prop -o "YES_IP_"$prop

fi

fi

done

sed 's/\"//g;s/\ //g;s/,//g' masternodelist_addr|grep -v "[{}]"> masternodelist_hash_addr_clear

cut -f2 -d":" masternodelist_addr|cut -f2 -d"\""|grep -v "[{}]"|sort > masternodelist_addr_only_sorted

mkdir upload
cp masternodelist_addr_only_sorted ./upload
cp masternodelist_hash_addr_clear ./upload

for fn in *_IP_*; do
if [ -s $fn ]
then
cp $fn ./upload
fi
done
cd upload

# Decompress the javascript helper functions and add HTML file.
dateis=`date -u +"%Y-%m-%d-%H-%M-%S"`
dateisAndTypes=`echo $dateis" <p>Note: We argue why we classified each type into mnowatch.org\\/Types<p>"`

filenameis="../the_results_dashd_"$dateis".html"


cat <<"EOF"|sed -e s/"MNOWATCH from dashd thedateis<\/title>"/"MNOwatch - FirstResults $dateis <\/title><link rel=\"icon\" type=\"image\/png\" href=\"favicon.ico\">"/g|sed -e s/"thedateis"/"$dateisAndTypes"/g > $filenameis
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>MNOWATCH from dashd thedateis</title>
<link rel="stylesheet" href="the_results_dashd_01.css">
<script src="the_results_dashd_01.js"></script>
</head>
<body onload="parseurl()">
<p><strong>
thedateis
</strong></p>
<p><strong>Click the headers to sort the table. BE PATIENT. The table is huge. You may have to press the javascript wait button of your browser.</strong></p>
IP fltr:<input type="text" id="fltr0" onkeyup="tableFilter(0)" placeholder="Search for names..">
YES fltr:<input type="text" id="fltr1" onkeyup="tableFilter(1)" placeholder="Search for names..">
NO fltr:<input type="text" id="fltr2" onkeyup="tableFilter(2)" placeholder="Search for names..">
ABS fltr:<input type="text" id="fltr3" onkeyup="tableFilter(3)" placeholder="Search for names..">
<br>
<div id="menu" class="hidden"></div>
<br>
<table id="table" class="table"> 
<tbody>
<tr>
<th>IP</th>
<th>YES VOTES</th>
<th>NO VOTES</th>
<th>ABS VOTES</th>
<th>VOTEHASH</th>
</tr>
EOF

sed -i '9i'"$codeurl2" $filenameis

csvfile=`echo $filenameis".csv"`
#a bug occurs to all proposals than contain a , in their proposal-name
#ex. proposal-name = VENEZUELAN-ALLIED-DASH-COMMUNITIES,Cash_Evolution_Bloomberg_Radio
> $csvfile
for gn in `cat masternodelist_hash_addr_clear`; do
		#BUG starts here
MNhashis=`echo $gn|cut -f1 -d":"`
ipis=`echo $gn|cut -f2 -d":"`
mycollat=`grep " $ipis$" ../collateraladdress_IP|cut -f1 -d" "`
#HCMN
myHCMNtype=`grep " $ipis:" ../collateraladdress_IPtype|cut -f3 -d" "`
myHCMNstatus=`grep " $ipis:" ../collateraladdress_IPtype|cut -f4 -d" "`
#end HCMN


#Below an alternative example on how to get the creation date of a collat address by bitinfocharts
#wget -qO- -U 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.6) Gecko/20070802 SeaMonkey/1.1.4' https://bitinfocharts.com/dash/address/Xy14MCXe7CL5nXJVtsHS7evs1X3jpkjgxM |awk -F"muted utc hidden-desktop" '{for(i=2;i<=NF;i++){{print $i}}}'|cut -f1 -d"<"|tail -1|cut -f2 -d">"

if [ $PREVIUSREPORTCOUNT -gt 1 ]
then
 SEARCHINPREVIUS=`grep ","$mycollat"," $PREVIUSREPORTFULL|wc -l`		
 if [ $SEARCHINPREVIUS -eq 1 ]
 then
  earlytxdate=`grep ","$mycollat"," $PREVIUSREPORTFULL|cut -f9 -d,`
 else
  #begin xkcd's contribution
  transact=$(dcli getaddresstxids '{"addresses": ["'$mycollat'"]}'|jq -r '.[0]')
  tx=$(dcli getrawtransaction $transact 1)
  blockis=$(jq -r '.locktime' <<< $tx)
  test -z "$blockis" && blockis=$(jq -r '.height' <<< "$tx")
  test "$blockis" -eq 0 && blockis=$(jq -r '.height' <<< "$tx")
  test -z "$blockis" && blockis=$(jq -r '.vout[0].spentHeight' <<< "$tx")
  blockhash=$(dcli getblockhash $blockis)
  mediantime="@"$(dcli getblock $blockhash|jq -r '.mediantime')
  earlytxdate=$(date -u +"%Y-%m-%d-%H-%M-%S" -d $mediantime)
 fi
else
 #begin xkcd's contribution
 transact=$(dcli getaddresstxids '{"addresses": ["'$mycollat'"]}'|jq -r '.[0]')
 tx=$(dcli getrawtransaction $transact 1)
 blockis=$(jq -r '.locktime' <<< $tx)
 test -z "$blockis" && blockis=$(jq -r '.height' <<< "$tx")
 test "$blockis" -eq 0 && blockis=$(jq -r '.height' <<< "$tx")
 test -z "$blockis" && blockis=$(jq -r '.vout[0].spentHeight' <<< "$tx")
 blockhash=$(dcli getblockhash $blockis)
 mediantime="@"$(dcli getblock $blockhash|jq -r '.mediantime')
 earlytxdate=$(date -u +"%Y-%m-%d-%H-%M-%S" -d $mediantime)
 #end xkcd's contribution
fi

# BUG: fix ipis in case this is not fixed: https://github.com/dashpay/dash/issues/2942

yesvotes=`grep -l ^$ipis$ *YES_IP_*|cut -f3- -d"_"`
yesvotes=`echo $yesvotes`
novotes=`grep -l ^$ipis$ *NO_IP_*|cut -f3- -d"_"`
novotes=`echo $novotes`
absvotes=`grep -l ^$ipis$ *ABSTAIN_IP_*|cut -f3- -d"_"`
absvotes=`echo $absvotes`

checkifitvoted=`grep -R $MNhashis ../*|grep gobject_getcurrentvotes|wc -l`
if [ $checkifitvoted -eq 0 ] 
then
checkmorethanoneIP=`grep $ipis":" masternodelist_hash_addr_clear|wc -l`
if [ $checkmorethanoneIP -gt 1 ] 
then
yesvotes=""
novotes=""
absvotes=""
fi
fi

allvotes=$yesvotes","$novotes","$absvotes
#there was a bug when hashing $allvotes, in case a person has NO votes and not ABS votes, while another person has not NO votes but has ABS votes identical to the previous person's NO votes. I tried to fix it by comma separate instead of space.
hashis=`bc <<<ibase=16\;$(sha1sum <<<$allvotes|tr a-z A-Z)0`


iscrowdnode2=`grep $mycollat $TYPES_DIR/CrowdNode.txt|wc -l`
if [ $iscrowdnode2 -eq 1 ]
then
 mytypeis="CrowdNode"
else
 num_matches=`grep -l $MNhashis $TYPES_DIR/*.txt|wc -l`
 if [ $num_matches -gt 1 ]
 then
  mytypeis="TypeMismatched"
 else
  if [ $num_matches -eq 1 ]
  then
   num_matches2=`grep -l $MNhashis $TYPES_DIR/*.txt`
   mytypeis=`basename $num_matches2|cut -f1 -d.`
  else
   mytypeis="NoType"
  fi
 fi
fi


echo "<tr><td class=\"container1\"><div><a target=\"_blank\" href=https://ipinfo.io/"$ipis">"$ipis"</a> "$MNhashis" <a target=\"_blank\" href=https://bitinfocharts.com/dash/address/"$mycollat">"$mycollat"</a> "$earlytxdate" "$mytypeis" "$myHCMNtype" "$myHCMNstatus"</div></td><td class=\"container2\"><div>"$yesvotes"</div></td><td class=\"container3\"><div>"$novotes"</div></td><td class=\"container4\"><div>"$absvotes"</div></td><td class=\"container5\"><div>"$hashis"</div></td></tr>" >> $filenameis


#mycollat and iscrowdnode caused problem to ssdeepit.sh but I fixed it 
#Crowdnode type is 1, Binance type is 2. Coinbase is 3.

#Question: Instead of numbers, can I use plain names(stings) for types into the csv files? 
#Will ssdeepit.sh and uniquehashvote report behave propery? I should investigate it.

echo "\"$ipis\",$MNhashis,$yesvotes,$novotes,$absvotes,\"$hashis\",$mycollat,$mytypeis,$earlytxdate,$myHCMNtype,$myHCMNstatus" >> $csvfile
done

cat <<"EOF" >> $filenameis
</tbody>
</table>
<script>
const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
const comparer = (idx, asc) => (a, b) => ((v1, v2) => v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2))(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {const table = th.closest('table');Array.from(table.querySelectorAll('tr:nth-child(n+2)')).sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc)).forEach(tr => table.appendChild(tr));})));
</script>
</body>
</html>  
EOF

# WARNING! In case of huge delays, we have to substitute td.innerText to td.innerHTML
cd ..

cp the_results_dashd_*.html.csv ../httpd
# Check it: diff plays in tty and not in cron!
# Warning! sometimes THERE iS diff in the_results.csv and there is no diff in the uniqueHashVotes.csv.
# This is because an mno who DID NOT voted appeared/left.

#In order not to use ls -trad that crashes the kernel, I have to change dirs
#BUGFIX:the code breaks ls! ---> compareresultfiles=`ls -trad $HTTPD_DIR/*|grep the_results_dashd.*.html.csv$|grep -v uniqueHashVotes|tail -2|wc -l`
compareresultfiles=`ls -tra $HTTPD_DIR/|grep the_results_dashd.*.html.csv$|grep -v .uniqueHashVotes.|tail -2|wc -l`

if [ $compareresultfiles -eq 2 ]
then
#BUGFIX: compareresultfiles=`ls -trad $HTTPD_DIR/*|grep the_results_dashd.*.html.csv$|grep -v uniqueHashVotes|tail -2`
compareresultfiles=`ls -tra $HTTPD_DIR/|grep the_results_dashd.*.html.csv$|grep -v .uniqueHashVotes.|tail -2`
#In order not to use ls -trad that crashes the kernel, I have to change dirs
cd $HTTPD_DIR
 istherediff=`diff $compareresultfiles |wc -l`
cd $TMP_DIR
 if [[ ( $superblock -eq 0 && $istherediff -eq 0 ) || ( $superblock -eq 1 && $istherediff -eq 0 ) ]]
 then
  echo $dateis" --> No diffs found between "$compareresultfiles" . "`date -u` > /tmp/Mnowatch_diffs
  #In order not to use ls -trad that crashes the kernel, I have to change dirs
  cd $HTTPD_DIR
  diff $compareresultfiles >> /tmp/Mnowatch_diffs 

  #In case $superblock -gt 0 then put endOFvote tag in the previus report.
  if [ $superblock -gt 0 ]
  then
   wheredoIedit=`grep -n "Everything below this line is updated by MNOWatch.sh, do not modify!" ./index.html |cut -f1 -d:`
   ADDTHIS="-<strong>EndOfVote</strong>"
   NUM=`expr $wheredoIedit + 7`
   checkit1=`sed "${NUM}q;d" ./index.html| grep "EndOfVote"|wc -l`
    if [ $checkit1 -eq 0 ]
     then
      checkit2=`sed "${NUM}q;d" ./index.html| grep "./leaderboard/analysis"|wc -l`
      if [ $checkit2 -eq 1 ]
       then
        sed -i "${NUM}s/$/-<strong>EndOfVote<\/strong>/" ./index.html
       else
        sed -i `expr $wheredoIedit + 7`'i'"$ADDTHIS" ./index.html
       fi
    fi
#$BIN_DIR/types.sh `ls -tra $HTTPD_DIR/*.uniqueHashVotes.*.csv|tail -1`
  fi

  cd $TMP_DIR
  deletelatest=`ls -tra $HTTPD_DIR/the_results_dashd_*.html.csv|grep -v .uniqueHashVotes.|tail -1`
  #WARNING. THE BELOW COMMAND IS EXTREMELY DANGERUS. MAKE SURE YOU ARE IN TMP_DIR
  { cd $TMP_DIR &&  rm -rf $deletelatest *_* upload proposals;} || echo "rm in $TMP_DIR has failed!"
  exit
 else
  echo $dateis" DIFFS FOUND! "$istherediff > /tmp/Mnowatch_diffs
#In order not to use ls -trad that crashes the kernel, I have to change dirs
cd $HTTPD_DIR
  diff $compareresultfiles >> /tmp/Mnowatch_diffs 
cd $TMP_DIR
 fi
else
 echo "I cant find two files to compare" > /tmp/Mnowatch_diffs
fi

filetimeis="upload_"$dateis".tar"
tar -cf $filetimeis ./upload
gzip -9 $filetimeis

distrfileis="distr_"$dateis".txt"

echo "$dateis" > $distrfileis
echo "The first operator includes all people who dont vote at all. All the rest are identified by the way they vote." >> $distrfileis

#cut -f24 -d"<" the_results_dashd_*.html|cut -f2 -d">"|grep -v [a-z]|grep -v [A-Z]| grep ^[0-9]|grep -v "-"|sort|uniq -c|sed -e s/'^   '/000/g|sed -s s/'000   '/000000/g|sed -e s/'000  '/00000/g|sed -s s/'000 '/0000/g|sort -r|cut -f1 -d" "|uniq -c|sed -e s/" 0"/" operator(s) control(s) "/g|sed -e s/$/" masternode(s)"/g >> $distrfileis
#xkcd replacement of the above command
grep -o '[0-9]\{40,\}' the_results_dashd_*.html|sort|uniq -c|awk '{ while (/^[[:blank:]]/) {if (sub(/^ /,"")) printf "0";}print;}'|sort -r|cut -f1 -d" "|uniq -c|sed 's/\( *[0-9]* \)000\(.*\)/\1operator(s) control(s) \2 masternode(s)/g' >> $distrfileis
#end of xkcd code

cp current_props ../httpd/current_props_"$dateis".txt
rm -f ../httpd/latestlink_currentprops.txt
ln -s current_props_"$dateis".txt ../httpd/latestlink_currentprops.txt

cp current_propshash ../httpd/current_propshash_"$dateis".txt
rm -f ../httpd/latestlink_currentpropshash.txt
ln -s current_propshash_"$dateis".txt ../httpd/latestlink_currentpropshash.txt

#cp upload_*.tar.gz ../httpd
#In case you want to keep the upload files, uncomment the above
cp distr_*.txt ../httpd
rm -f ../httpd/latestlink_distr.txt
ln -s distr_$dateis.txt ../httpd/latestlink_distr.txt

cp the_results_dashd_*.html ../httpd
rm -f ../httpd/latestlink_theresultsdashd.html
ln -s the_results_dashd_$dateis.html ../httpd/latestlink_theresultsdashd.html


wheredoIedit=`grep -n "Everything below this line is updated by MNOWatch.sh, do not modify!" ../httpd/index.html |cut -f1 -d:`
ADDTHIS="<br><a href=\""`ls ./distr_*.txt`"\"> the distribution $dateis </a> and <a href=\""`ls ./the_results*.html`"\"> the results $dateis </a> (<a href=\""`ls ./the_results*.html.csv`"\">csv</a>)" 
sed -i `expr $wheredoIedit + 1`'i'"$ADDTHIS" ../httpd/index.html

if [ $SIMILARNUM -gt 0 ]
then
$BIN_DIR/ssdeepit.sh `ls -tra $HTTPD_DIR/*html.csv|tail -1` $SIMILARNUM
cp the_results_dashd_*.similar.*.csv ../httpd
cp the_results_dashd_*.similar.*.html ../httpd
ADDTHIS=" and <a href=\""`ls ./the_results*.similar.*.csv`"\">the similarities.csv</a> (<a href=\""`ls ./the_results*.similar.*.html`"\">html</a>)" 
else
$BIN_DIR/ssdeepit.sh `ls -tra $HTTPD_DIR/*html.csv|tail -1`
ADDTHIS=" and didn't calculate similarites" 
fi
sed -i `expr $wheredoIedit + 2`'i'"$ADDTHIS" ../httpd/index.html

cp the_results_dashd_*.uniqueHashVotes.*.csv ../httpd

rm -f ../httpd/latestlink_DashdUniqueHashVotes.html
cp the_results_dashd_*.uniqueHashVotes.*.html ../httpd
#the soft link should be added only at the end.

ADDTHIS=" and <a href=\""`ls ./the_results*.uniqueHashVotes.*.csv`"\"> the uniqueVotesHashes.csv</a>"
sed -i `expr $wheredoIedit + 3`'i'"$ADDTHIS" ../httpd/index.html
ADDTHIS="(<span style=\"background: #00ee00\"><a href=\""`ls ./the_results*.uniqueHashVotes.*.html`"\">html</a></span>)"
sed -i `expr $wheredoIedit + 4`'i'"$ADDTHIS" ../httpd/index.html
cd $TMP_DIR; rm -rf *_* upload proposals

#here I change the working  directory to httpd 
cd $HTTPD_DIR

diffis=`ls -ltra|grep .uniqueHashVotes.|tail -1|cut -f4 -d"_"|cut -f1 -d"."`.diff
filestodiff=`ls -lrta |grep .uniqueHashVotes.|grep -v html |tail -2|cut -f2 -d":"|cut -f2 -d" "|wc -l`
if [ $filestodiff -eq 2 ]
then
#TO DO: debug git diff --color-words. It seems to put some IPs in the same line while we expect to be in separete lines.
 file1todiff=`ls -lrta |grep .uniqueHashVotes.|grep -v html |tail -2|cut -f2 -d":"|cut -f2 -d" "|head -1`
 file2todiff=`ls -lrta |grep .uniqueHashVotes.|grep -v html |tail -1|cut -f2 -d":"|cut -f2 -d" "`
 cat $file1todiff |cut -f1-11 -d, > $TMP_DIR/$file1todiff
 cat $file2todiff |cut -f1-11 -d, > $TMP_DIR/$file2todiff

 cd $TMP_DIR
#WARNING. Diff should not take into account the last collumn of history
 git diff --color-words --word-diff=plain --unified=0 $file1todiff $file2todiff > $HTTPD_DIR/$diffis
 cd $HTTPD_DIR
 rm -f $TMP_DIR/$file1todiff
 rm -f $TMP_DIR/$file2todiff
else
 echo "" > $diffis
fi

ADDTHIS=" and <a href=\"./"$diffis"\">the git diff</a>"
sed -i `expr $wheredoIedit + 5`'i'"$ADDTHIS" ./index.html
#TO DO: debug ansi2html when looks like a table.
cat $diffis |$BIN_DIR/ansi2html.sh > $diffis.init.html

initfile="$HTTPD_DIR/$diffis.init.html"
targetfile="$HTTPD_DIR/$diffis.html"
tblstart=`grep -n ".csv</span>" $initfile |tail -1|cut -f1 -d:`
#TO DO: If git_diff is empty do not create a link of it in index.html, but rathere present the simple diff (which will contain the new IPs that appeared  who didnt vote at all,  but caused the new report to be triggered)
head -n $tblstart $initfile 2>/dev/null > $targetfile
echo "<style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; } </style><table><thead><tr><th>IPS</th><th>YES_VOTES</th><th>NO_VOTES</th><th>ABSTAIN_VOTES</th><th>VOTESHASH</th><th>IPSHASH</th><th>NUM_OF_MNS</th><th>MNS</th></tr></thead><tbody><tr><td><div>" >> $targetfile
tblstart=`expr $tblstart + 1`
#TO DO: insert in every line the VOTEHASH as the keyword of the row <tr id=$VOTEHASH>. (how to do it without looping? maybe by using the command paste)
tail -n +$tblstart $initfile|grep -v "<span class=\"f6\">@@"|sed -e s/"^&quot;"/"<\/div><\/td><\/tr><tr><td><div>\&quot;"/g|sed -e s/"^<span class=\"f1\">"/"<\/div><\/td><\/tr><tr><td><div><span class=\"f1\">"/g|sed -e s/"^<span class=\"f2\">"/"<\/div><\/td><\/tr><tr><td><div><span class=\"f2\">"/g|sed -e s/,/"<\/div><\/td><td><div>"/g >> $targetfile
tblend=`grep -n "</pre>" $targetfile |tail -1|cut -f1 -d:`
ADDTHIS="</tr></td></tbody></table>"
tblend=$tblend"i$ADDTHIS"
sed -i $tblend $targetfile
ADDTHIS="<title>MNOwatch - Diff $dateis</title><link rel=\"icon\" type=\"image/png\" href=\"favicon.ico\">"
sed -i '3i'"$ADDTHIS" $targetfile
rm $initfile

ADDTHIS=" (<a href=\"./"$diffis.html"\">html</a>)"
sed -i `expr $wheredoIedit + 6`'i'"$ADDTHIS" ./index.html

if [ $superblock -gt 0 ]
then
ADDTHIS="-<strong>EndOfVote</strong>"
sed -i `expr $wheredoIedit + 7`'i'"$ADDTHIS" ./index.html
fi

reg2='^leaderboard '
if [[ $MYCOMMENT =~ $reg2 ]] 
then
ADDTHIS="-<strong><a href=\"./leaderboard/analysis/?"`echo $MYCOMMENT|cut -f2 -d" "`"\">Leaderboard</a></strong>"
sed -i `expr $wheredoIedit + 7`'i'"$ADDTHIS" ./index.html
fi


#the below soft link should be added only here otherwise it causes bugs

#echo "adding link"
ln -s `ls -tra ./the_results_dashd_*.uniqueHashVotes.*.html|tail -1` ./latestlink_DashdUniqueHashVotes.html

#WARNING: The below scripts warnings.sh and types.sh have not been published yet as opensource

#SEND THE WARNING MAILS TO THE SUBSCRIBERS
#enabled_MN=`dcli masternode count | jq .enabled`
#$MYHOME_DIR/warnings/warnings.sh $dateis $enabled_MN

#DEFINE THE AUTOMATED TYPES/CLUSTERS
#We could run types.sh after the endofVote

#put the below at the end of a the start?
#if [ $superblock -gt 0 ]
# then
#$BIN_DIR/types.sh `ls -tra $HTTPD_DIR/*.uniqueHashVotes.*.csv|tail -1`
#fi

#echo "THE END! "
