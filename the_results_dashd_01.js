function tableFilter(n) {
	var input, filter, table, tr, td, i;
	input = document.getElementById("fltr"+n);
	filter = input.value.toUpperCase();
	table = document.getElementById("table");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[n];
		if (td) {
		if (td.innerHTML.toUpperCase().indexOf(filter) > -1)
			tr[i].style.display = "";
		else
			tr[i].style.display = "none";
		}
	}
}


document.addEventListener('DOMContentLoaded',()=>{
	document.querySelector('#fltr0').onkeyup = ()=>{tableFilter(0)}
});

document.addEventListener('DOMContentLoaded',()=>{
	document.querySelector('#fltr1').onkeyup = ()=>{tableFilter(1)}
});
document.addEventListener('DOMContentLoaded',()=>{
	document.querySelector('#fltr2').onkeyup = ()=>{tableFilter(2)}
});
document.addEventListener('DOMContentLoaded',()=>{
	document.querySelector('#fltr3').onkeyup = ()=>{tableFilter(3)}
});

document.addEventListener('DOMContentLoaded', function() {
	const menu = document.getElementById('menu');
	const table = document.getElementById('table');
	const headers = [].slice.call(table.querySelectorAll('th'));
	const cells = [].slice.call(table.querySelectorAll('th, td'));
	const numColumns = headers.length;
	const tbody = table.querySelector('tbody');
	tbody.addEventListener('contextmenu', function(e) {
		e.preventDefault();
		const rect = tbody.getBoundingClientRect();
		const x = e.clientX - rect.left;
		const y = e.clientY - rect.top;
		menu.style.top = `${y}px`;
		menu.style.left = `${x}px`;
		menu.classList.toggle('hidden');
	});
	const showColumn = function(index) {
		cells
			.filter(function(cell) {
				return cell.getAttribute('data-column-index') === `${index}`;
			})
			.forEach(function(cell) {
				cell.style.display = '';
				cell.setAttribute('data-shown', 'true');
			});
		menu.querySelectorAll(`[type="checkbox"][disabled]`).forEach(function(checkbox) {
			checkbox.removeAttribute('disabled');
		});
	};
	const hideColumn = function(index) {
		cells
			.filter(function(cell) {
				return cell.getAttribute('data-column-index') === `${index}`;
			})
			.forEach(function(cell) {
				cell.style.display = 'none';
				cell.setAttribute('data-shown', 'false');
			});
		// How many columns are hidden
		const numHiddenCols = headers
			.filter(function(th) {
				return th.getAttribute('data-shown') === 'false';
			})
			.length;
		if (numHiddenCols === numColumns - 1) {
			// There's only one column which isn't hidden yet
			// We don't allow user to hide it
			const shownColumnIndex = tbody.querySelector('[data-shown="true"]').getAttribute('data-column-index');
			const checkbox = menu.querySelector(`[type="checkbox"][data-column-index="${shownColumnIndex}"]`);
			checkbox.setAttribute('disabled', 'true');
		}
	};
	cells.forEach(function(cell, index) {
		cell.setAttribute('data-column-index', index % numColumns);
		cell.setAttribute('data-shown', 'true');
	});
	headers.forEach(function(th, index) {
		// Build the menu item
		const label = document.createElement('label');
		const checkbox = document.createElement('input');
		checkbox.setAttribute('type', 'checkbox');
		checkbox.setAttribute('checked', 'true');
		checkbox.setAttribute('data-column-index', index);
		checkbox.style.marginRight = '.25rem';
		const text = document.createTextNode(th.textContent);
		label.appendChild(text);
		menu.appendChild(checkbox);
		menu.appendChild(label);
		// Handle the event
		checkbox.addEventListener('change', function(e) {
			e.target.checked ? showColumn(index) : hideColumn(index);
			menu.classList.add('hidden');
		});
	});
});
function parseurl() {
 var sPageURL = window.location.search.substring(1);
 var sURLVariables = sPageURL.split("=");
 var filteris = sURLVariables[0];
 var valueis = sURLVariables[1];
 document.getElementById("fltr"+filteris).value = valueis;
 tableFilter(filteris);
}

